{%- from "ipset-blacklist/map.jinja" import ipset with context %}
ipset_cfg_dir:
  file.directory:
    - name: /usr/local/etc/ipset
    - user: root
    - group: root
    - mode: 0700

ipset_cfg_sets:
  file.managed:
    - name: /usr/local/etc/ipset/sets.conf
    - source: salt://ipset-blacklist/files/ipset-sets.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 0600
    - sets: {{ ipset.sets | yaml }}
    - require:
      - file: ipset_cfg_dir

ipset_cfg_members:
  file.managed:
    - name: /usr/local/etc/ipset/members.conf
    - source: salt://ipset-blacklist/files/ipset-members.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: 0600
    - sets: {{ ipset.sets | yaml }}
    - require:
      - file: ipset_cfg_dir

ipset_blacklist_directory:
  file.directory:
    - name: /etc/ipset-blacklist-pub
    - user: root
    - group: root
    - makedirs: True

ipset_blacklist_script_config:
  file.managed:
    - name: /etc/ipset-blacklist-pub/ipset-blacklist.conf
    - source: salt://ipset-blacklist/files/ipset-blacklist.conf
    - user: root
    - group: root

ipset_blacklist_script:
  file.managed:
    - name: /usr/local/sbin/update-blacklist.sh
    - source: salt://ipset-blacklist/files/update-blacklist.sh
    - user: root
    - group: root

ipset_make_executable:
  cmd.run:
    - name: chmod +x /usr/local/sbin/update-blacklist.sh