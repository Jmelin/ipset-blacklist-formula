# -*- coding: utf-8 -*-
# vim: ft=sls
# Init ipset-blacklist
{%- from "ipset-blacklist/map.jinja" import ipset with context %}

{% if ipset.enabled %}
include:
  - ipset-blacklist.install
  - ipset-blacklist.config
  - ipset-blacklist.service
{%- else %}
'ipset-blacklist-formula disabled':
  test.succeed_without_changes
{%- endif %}

