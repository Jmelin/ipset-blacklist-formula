{%- from "ipset-blacklist/map.jinja" import ipset with context %}
ipset_service_reload:
  cmd.run:
    - name: systemctl reload ipset
    - onchanges:
      - file: ipset_cfg_members
    - require:
      - service: ipset_service

ipset_service:
  service.{{ ipset.service.state }}:
    - name: ipset
    - enable: {{ ipset.service.enable }}
    - watch:
      - file: ipset_cfg_sets
    - require:
      - pkg: ipset_package
      - file: ipset_service_install
      - file: ipset_cfg_members

ipset_blacklist_first_run:
  cmd.run:
    - name: /usr/local/sbin/update-blacklist.sh /etc/ipset-blacklist-pub/ipset-blacklist.conf

ipset_blacklist_first_restore:
  cmd.run:
    - name: ipset restore < /etc/ipset-blacklist-pub/ip-blacklist.restore

ipset_blacklist_set_iptables_drop:
  cmd.run:
    - name: iptables -I INPUT 1 -m set --match-set blacklist-pub src -j DROP

