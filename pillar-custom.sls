ipset:
  enabled: True
  service:
    state: running
    enable: false
  sets:
    loopback:
        type: hash:ip
        members:
          - 127.0.0.1
          - 127.0.0.2
    my-link-local-blocks:
      type: hash:net
      family: inet6
      members:
        - fe80::/64
        - fe80:1::/64
        - fe80:2::/64
        - fe80:3::/64